extern crate a2;
extern crate futures;
extern crate tokio;

use std::{
    sync::{Arc, RwLock, Weak},
    collections::{HashMap, HashSet, VecDeque},
};

use a2::{
    client::{Client, FutureResponse},
    request::payload::Payload,
    error::Error,
};

use futures::{
    future,
    sync::oneshot,
    Future,
    Poll,
    Async,
};

pub type ConnFuture = future::Either<future::FutureResult<String, Error>, Box<Future<Item=Client, Error=Error> + Send>>;

pub struct Checkout {
    key: String,
    pool: Arc<RwLock<Pool>>,
    parked: Option<oneshot::Receiver<String>>,
}

impl Checkout {
    fn poll_parked(&mut self) -> Poll<Option<String>, Error> {
        if let Some(ref mut rx) = self.parked {
            match rx.poll() {
                Ok(Async::Ready(value)) => {
                    Ok(Async::Ready(Some(self.pool.reuse())))
                }
            }
        }
    }
}

pub struct PoolInner {
    connecting: HashSet<String>,
    idle: HashMap<String,String>,
    parked: HashMap<String, VecDeque<oneshot::Sender<String>>>
}

pub struct Pool {
    inner: Arc<RwLock<PoolInner>>,
}

impl Clone for Pool {
    fn clone(&self) -> Self {
        Pool {
            inner: self.inner.clone()
        }
    }
}

impl Pool {
    pub fn new() -> Pool {
        Pool {
            connecting: HashSet::new(),
            inner: Arc::new(RwLock::new(HashMap::new()))
        }
    }

    pub fn get(&self, app_id: &str) -> ConnFuture {
        if let Some(conn) = self.inner.get(app_id) {
            future::Either::A(future::ok(conn))
        } else {
            future::E1
        }
    }
}

fn main() {
    println!("Hello, world!");
}
